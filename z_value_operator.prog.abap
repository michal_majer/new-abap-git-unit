REPORT z_value_operator.

TYPES: BEGIN OF lt_s_invoice_header,
         number TYPE vbeln,
       END OF lt_s_invoice_header.

TYPES: BEGIN OF lt_s_invoice_position,
         position TYPE posnr,
         material TYPE matnr,
       END OF lt_s_invoice_position.

TYPES: BEGIN OF lt_s_invoice,
         head     TYPE lt_s_invoice_header,
         position TYPE lt_s_invoice_position,
       END OF lt_s_invoice.

DATA: ls_invoice_header   TYPE lt_s_invoice_header,
      ls_invoice_position TYPE lt_s_invoice_position.

" # - dynamic typicing
ls_invoice_header = VALUE #( number = '1234567890' ).
ls_invoice_header = VALUE lt_s_invoice_header( number = '1234567890' ).


ls_invoice_position = VALUE lt_s_invoice_position( position = '000010' material = '100000000').
*DATA(lv_test) = VALUE #( position = '000010' material = '100000000'). Error - not possible
DATA(lv_inline_invoice_position) = VALUE lt_s_invoice_position( position = '000010' material = '100000000').

DATA(lv_invoice) = VALUE lt_s_invoice( head-number = '123' position-position = '000010' position-material = '100000000').

**********************************************************************
* TABLES
**********************************************************************

TYPES lt_t_invoice_header TYPE TABLE OF lt_s_invoice_header.
TYPES lt_t_invoice TYPE TABLE OF lt_s_invoice.
DATA lt_invoice_header TYPE lt_t_invoice_header.
DATA lt_invoice TYPE lt_t_invoice.

lt_invoice_header = VALUE #( ( ) ). " [ ]
lt_invoice_header = VALUE #( ( number = '1') ). " [ 1 ]
lt_invoice_header = VALUE #( ( number = '1') ( number = '1') ). " [ 1, 1 ]
lt_invoice_header = VALUE #( ( number = '1') ( number = '1') ( number = '3') ). " [ 1, 1, 3]

lt_invoice = VALUE #( ( head-number = '10' ) ( position-position = '10' position-material = '50' ) ).
