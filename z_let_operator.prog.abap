REPORT z_let_operator.

TYPES: BEGIN OF ty_invoice,
         number   TYPE vbeln,
         position TYPE posnr,
         ftype TYPE fkart,
         net   TYPE netwr,
         vat   TYPE netwr,
         gross TYPE netwr,
         text TYPE string,
       END OF ty_invoice.

TYPES: lt_t_invoice TYPE STANDARD TABLE OF ty_invoice.

DATA: lt_invoice TYPE lt_t_invoice,
      lt_invoice_fkart TYPE lt_t_invoice,
      lt_invoice_z001 TYPE lt_t_invoice,
      lt_invoice_gross TYPE lt_t_invoice.

lt_invoice = VALUE #( ( number = '1000000000' position = '000010' ftype = 'Z001' net = '1000' vat = '230' )
                      ( number = '1000000000' position = '000011' ftype = 'Z001' net = '50' vat = '2' )
                      ( number = '1000000001' position = '000010' ftype = 'Z002')
                      ( number = '1000000002' position = '000010' ) ).


DO lines( lt_invoice ) TIMES.
 lt_invoice[ sy-index ]-text = CONV string(
    LET net = lt_invoice[ sy-index ]-net
        vat = lt_invoice[ sy-index ]-vat
        gross = CONV netwr( net + vat )
        currency = 'PLN'
     IN |Kwota netto: { net } { currency }, vat: { vat } { currency } = { gross } { currency }|
 ).
 WRITE: lt_invoice[ sy-index ]-text, /.
ENDDO.
