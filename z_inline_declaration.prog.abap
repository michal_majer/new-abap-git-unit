REPORT z_inline_declaration.

CLASS lcl_example DEFINITION.
  PUBLIC SECTION.
    METHODS constructor IMPORTING iv_char TYPE char1.
    METHODS return_string RETURNING VALUE(rv_string) TYPE string.
    METHODS export_string EXPORTING ev_string TYPE string.
  PRIVATE SECTION.

ENDCLASS.

CLASS lcl_example IMPLEMENTATION.
  METHOD constructor.
    DATA(lv_char1) = iv_char.
  ENDMETHOD.

  METHOD return_string.
    rv_string = 'This is return'.
  ENDMETHOD.

  METHOD export_string.
    ev_string = 'This is export'.
  ENDMETHOD.
ENDCLASS.

AT SELECTION-SCREEN.

**********************************************************************
* Before ABAP 7.40
**********************************************************************
  DATA lv_char TYPE c LENGTH 1.
  lv_char = 'A'.

  DATA lv_string TYPE string.
  lv_string = 'This is string'.

  DATA lv_int TYPE i.
  lv_int = 100.

  DATA lv_float TYPE f.
  lv_float = '100.25'.

**********************************************************************
* With  ABAP 7.40
**********************************************************************
  DATA(lv_inline_char) = 'A'.
  DATA(lv_inline_string) = 'This is string'.
  DATA(lv_inline_int) = 100.
  DATA(lv_inline_float) = '100.25'.
  DATA(lv_inline_string_length) = strlen( lv_inline_string ).
  ASSIGN 'test' TO FIELD-SYMBOL(<fs_test>).

  DATA(lo_example) = NEW lcl_example( 'A' ). "Create new object with NEW
  DATA(lv_return_string) = lo_example->return_string( ).
  lo_example->export_string( IMPORTING ev_string = DATA(lv_import_string) ).

  SELECT * FROM crmd_orderadm_h INTO TABLE @DATA(lt_tickets).

  LOOP AT lt_tickets INTO DATA(ls_ticket).
  ENDLOOP.

  LOOP AT lt_tickets ASSIGNING FIELD-SYMBOL(<fs_ticket>).
  ENDLOOP.

* No possible is to use inline declaration in function module:
*  CALL FUNCTION 'READ_TEXT'
*    EXPORTING
*      id       = 'ST'
*      name     = 'ADRNR'
*      object   = 'TEXT'
*      language = 'EN'
*    IMPORTING
*      header = DATA(lt_head). "Can't do :('
