"! Basic usage of ABAP doc
CLASS zcl_abap_unit_example DEFINITION

  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    METHODS: constructor IMPORTING iv_num TYPE i OPTIONAL.
    METHODS: return_5 RETURNING VALUE(rv_5) TYPE i.
    METHODS: return_string RETURNING VALUE(rv_string) TYPE string.
    METHODS: get_tickets.
  PROTECTED SECTION.
  PRIVATE SECTION.
    DATA: mv_num TYPE i.


ENDCLASS.



CLASS zcl_abap_unit_example IMPLEMENTATION.

  METHOD constructor.
    mv_num = iv_num.
  ENDMETHOD.



  METHOD get_tickets.
    DATA exc TYPE REF TO cx_sy_dynamic_osql_semantics.
    SELECT * FROM crmd_orderadm_h INTO TABLE @DATA(lt_tickets)
    WHERE process_type = '123'.
    "Return sy-subrc
  ENDMETHOD.

  METHOD return_5.
    rv_5 = 5.
  ENDMETHOD.

  METHOD return_string.
    rv_string = 'This is 25 string.'.
  ENDMETHOD.
ENDCLASS.
