REPORT z_conditions.

DATA(lv_number) = 99.
DATA(lv_status) = COND #(
    WHEN lv_number >= 0 AND lv_number <= 9 THEN 'Easy'
    WHEN lv_number >= 9 AND lv_number <= 56 THEN 'Normal'
    ELSE 'Hard'
).

WRITE: lv_status.

DATA(lv_string_month) = 'Grudzień'.
DATA(lv_month) = SWITCH #( lv_string_month
    WHEN 'Styczeń' OR 'Sty' THEN 1
    WHEN 'Luty' OR 'Lu' THEN 2
    WHEN 'Marzec' OR 'Mar' THEN 3
    ELSE 0
).

WRITE: /, lv_month.
