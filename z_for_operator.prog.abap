REPORT z_for_operator.

TYPES: BEGIN OF ty_invoice,
         number   TYPE vbeln,
         position TYPE posnr,
         ftype TYPE fkart,
         net   TYPE netwr,
         vat   TYPE netwr,
         gross TYPE netwr,
       END OF ty_invoice.

TYPES: lt_t_invoice TYPE STANDARD TABLE OF ty_invoice.

DATA: lt_invoice TYPE lt_t_invoice,
      lt_invoice_fkart TYPE lt_t_invoice,
      lt_invoice_z001 TYPE lt_t_invoice,
      lt_invoice_gross TYPE lt_t_invoice.

lt_invoice = VALUE #( ( number = '1000000000' position = '000010' ftype = 'Z001' net = '1000' vat = '230' ) ( number = '1000000000' position = '000011' ftype = 'Z001' net = '50' vat = '2' )
                      ( number = '1000000001' position = '000010' ftype = 'Z002')
                      ( number = '1000000002' position = '000010' ) ).

lt_invoice_fkart = VALUE #( FOR ls_invoice IN lt_invoice WHERE ( ftype <> '' ) ( ls_invoice ) ).

lt_invoice_z001  = VALUE #( FOR ls_invoice IN lt_invoice WHERE ( ftype = 'Z001' ) ( ls_invoice ) ).

lt_invoice_gross = VALUE #( FOR ls_invoice IN lt_invoice_z001 ( number = ls_invoice-number
                                                              position = ls_invoice-position
                                                                 ftype = ls_invoice-ftype
                                                                   net = ls_invoice-net
                                                                   vat = ls_invoice-vat
                                                                 gross = ls_invoice-net + ls_invoice-vat ) ).

lt_invoice_gross = VALUE #( FOR ls_invoice IN lt_invoice_z001 WHERE ( gross > 100 ) ( number = ls_invoice-number
                                                              position = ls_invoice-position
                                                                 ftype = ls_invoice-ftype
                                                                   net = ls_invoice-net
                                                                   vat = ls_invoice-vat
                                                                 gross = ls_invoice-net + ls_invoice-vat ) ).

lt_invoice_gross = VALUE #( FOR ls_invoice IN lt_invoice_z001 WHERE ( net > 100 ) ( number = ls_invoice-number
                                                              position = ls_invoice-position
                                                                 ftype = ls_invoice-ftype
                                                                   net = ls_invoice-net
                                                                   vat = ls_invoice-vat
                                                                 gross = ls_invoice-net + ls_invoice-vat ) ).

BREAK-POINT.
