REPORT z_strings.

WRITE: 'This is String'.
WRITE: /.
WRITE: |This is string in template|.

DATA(lv_number) = 1523.
WRITE: | Your number: { lv_number }.|.
WRITE: /.

DATA: amount_field   TYPE netwr VALUE '1234567.123',
      country        TYPE tcp0c-country VALUE 'PL',
      currency_field TYPE waerk.

WRITE: |{ amount_field CURRENCY = currency_field  NUMBER = USER }|.
WRITE: |{ amount_field CURRENCY = currency_field  COUNTRY = country }|.
*character_string = |{ amount_field COUNTRY = 'GB  ' }|.

DATA matnr TYPE matnr VALUE '12'.
WRITE: /, matnr, /.
WRITE |{ matnr ALPHA = IN }|. "Add leading zeros
WRITE |{ matnr ALPHA = OUT }|. "Remove leading zeros

"Simple XML build:
DATA(lv_xml) = |<xml><value>{ matnr ALPHA = IN }</value></xml>|.
WRITE lv_xml.

"Concatenation
DATA(lv_merge_xml) = lv_xml && lv_xml.
WRITE: /, lv_merge_xml, /.

DATA(lv_matnr) = |{ matnr ALPHA = IN }|.
WRITE: /, lv_matnr.



" Embedded Expressions
*    [WIDTH     = len]
*    [ALIGN     = LEFT|RIGHT|CENTER|(val)]
*    [PAD       = c]
*    [CASE      = RAW|UPPER|LOWER|(val)]
*    [SIGN      = LEFT|LEFTPLUS|LEFTSPACE|RIGHT|RIGHTPLUS|RIGHTSPACE|(val)]
*    [EXPONENT  = exp]
*    [DECIMALS  = dec]
*    [ZERO      = YES|NO|(val)]
*    [XSD       = YES|NO|(val)]
*    [STYLE     =  SIMPLE|SIGN_AS_POSTFIX|SCALE_PRESERVING
*                 |SCIENTIFIC|SCIENTIFIC_WITH_LEADING_ZERO
*                 |SCALE_PRESERVING_SCIENTIFIC|ENGINEERING
*                 |(val)]
*    [CURRENCY  = cur]
*    [NUMBER    = RAW|USER|ENVIRONMENT|(val)]
*    [DATE      = RAW|ISO|USER|ENVIRONMENT|(val)]
*    [TIME      = RAW|ISO|USER|ENVIRONMENT|(val)]
*    [TIMESTAMP = SPACE|ISO|USER|ENVIRONMENT|(val)]
*    [TIMEZONE  = tz]
*    [COUNTRY   = cty] ...
