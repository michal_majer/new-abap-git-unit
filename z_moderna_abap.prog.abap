REPORT z_moderna_abap.

SELECT * FROM crmd_orderadm_h INTO TABLE @DATA(lt_tickets).

WRITE: lt_tickets[ 1 ]-object_id, /.
WRITE: |Numer zgloszenia to: { lt_tickets[ 1 ]-object_id }|, /.
WRITE: |Numer zgloszenia to: { lt_tickets[ 1 ]-object_id ALPHA = OUT }|, /.

*DATA: lt_object_guid TYPE crmt_object_guid_tab,
*      ls_object_guid TYPE crmt_object_guid.
*
*ls_object_guid = lt_tickets[ 1 ]-guid.
*APPEND ls_object_guid TO lt_object_guid.

DATA(lt_object_guid) = VALUE crmt_object_guid_tab( ( CONV #( lt_tickets[ 1 ]-guid ) ) ).
DATA lt_orderadm_h TYPE crmt_orderadm_h_wrkt.

CALL FUNCTION 'CRM_ORDER_READ'
    EXPORTING
        it_header_guid = lt_object_guid
    IMPORTING
        et_orderadm_h = lt_orderadm_h.
IF sy-subrc = 0.
    WRITE: 'Function Call OK'.
ELSE.
    WRITE: 'Function Call Error'.
ENDIF.

LOOP AT lt_tickets INTO DATA(ls_ticket).
    WRITE: | { ls_ticket-object_id } - { ls_ticket-posting_date } |, /.
ENDLOOP.

DATA lt_tickets_from_21 TYPE STANDARD TABLE OF crmt_object_id_db.
lt_tickets_from_21 = VALUE #( FOR ticket IN lt_tickets WHERE ( posting_date = '20170621' ) ( ticket-object_id ) ).

LOOP AT lt_tickets_from_21 INTO DATA(ls_ticket_from_21).
    WRITE: | { ls_ticket_from_21 } |, /.
ENDLOOP.
