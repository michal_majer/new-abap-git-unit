*"* use this source file for your ABAP unit test classes

CLASS ltc_abap_unit_example DEFINITION FINAL FOR TESTING
  DURATION SHORT
  RISK LEVEL HARMLESS.

  PUBLIC SECTION.

  PRIVATE SECTION.
    DATA: lo_unit_example TYPE REF TO zcl_abap_unit_example,
          not_lo_unit_example TYPE REF TO zcl_abap_unit_example.
    CLASS-METHODS class_setup.
    METHODS setup.
    METHODS assert_equal FOR TESTING.
    METHODS assert_differs FOR TESTING.
    METHODS assert_bound FOR TESTING.
    METHODS assert_not_bound FOR TESTING.
    METHODS assert_initial FOR TESTING.
    METHODS assert_not_initial FOR TESTING.
    METHODS assert_char_cp FOR TESTING.
    METHODS assert_char_np FOR TESTING.
    METHODS assert_table_contains FOR TESTING.
    METHODS assert_number_between FOR TESTING.
    METHODS assert_subrc FOR TESTING.
    METHODS assert_equals_float FOR TESTING.
ENDCLASS.


CLASS ltc_abap_unit_example IMPLEMENTATION.
  METHOD class_setup.

  ENDMETHOD.

  METHOD setup.
    lo_unit_example = new zcl_abap_unit_example( ).
  ENDMETHOD.



  METHOD assert_equal.
    cl_abap_unit_assert=>assert_equals(
        act = lo_unit_example->return_5( )
        exp = 5
        msg = 'It should return 5'
    ).
  ENDMETHOD.

  METHOD assert_differs.
    cl_abap_unit_assert=>assert_differs(
        act = lo_unit_example->return_5( )
        exp = 10
        msg = 'It should return 5'
    ).
  ENDMETHOD.

  METHOD assert_bound.
    cl_abap_unit_assert=>assert_bound(
        act = lo_unit_example
        msg = 'Object must be bound'
    ).
  ENDMETHOD.

  METHOD assert_not_bound.
    cl_abap_unit_assert=>assert_not_bound(
        act = not_lo_unit_example
        msg = 'Object must be unbound'
    ).
  ENDMETHOD.

  METHOD assert_initial.
    cl_abap_unit_assert=>assert_initial(
        act = ''
        msg = 'It should be initial'
    ).
  ENDMETHOD.

  METHOD assert_not_initial.
    cl_abap_unit_assert=>assert_not_initial(
        act = lo_unit_example->return_5( )
        msg = 'It should not be initial'
    ).
  ENDMETHOD.

  METHOD assert_char_cp.
    cl_abap_unit_assert=>assert_char_cp(
        act = lo_unit_example->return_string( )
        exp = '*25*'
        msg = 'It should be string with pattern /*25*/'
    ).
  ENDMETHOD.

  METHOD assert_char_np.
    cl_abap_unit_assert=>assert_char_np(
        act = lo_unit_example->return_string( )
        exp = '*1*'
        msg = 'It should be string without pattern /*1*/'
    ).
  ENDMETHOD.

  METHOD assert_table_contains.
    TYPES: BEGIN OF lts_i,
           number TYPE i,
           text TYPE string,
           END OF lts_i.

    TYPES ltt_numbers TYPE STANDARD TABLE OF lts_i WITH KEY number.

    DATA(lt_numbers) = VALUE ltt_numbers( ( number = 1 ) ( number = 2 ) ( number = 3 ) ).
    DATA(ls_line) = VALUE lts_i( number = 3 ).

    DATA(lt_numbers2) = VALUE ltt_numbers( ( number = 1 ) ( number = 2 ) ( number = 3 text = '' ) ).
    DATA(ls_line2) = VALUE lts_i( number = 3 ).

    cl_abap_unit_assert=>assert_table_contains(
        line = ls_line
        table = lt_numbers
        msg = 'It should contain row in table'
    ).

    cl_abap_unit_assert=>assert_table_contains(
        line = ls_line2
        table = lt_numbers2
        msg = 'It should contain row in table'
    ).
  ENDMETHOD.

  METHOD assert_number_between.
    cl_abap_unit_assert=>assert_number_between(
        lower = 1
        upper = 10
        number = lo_unit_example->return_5( )
        msg = 'It should return number between 1 and 10'
    ).
  ENDMETHOD.

  METHOD assert_subrc.
*    lo_unit_example->get_tickets( ).
*    cl_abap_unit_assert=>assert_subrc(
*        exp = 4
*        msg = 'It should be 4 status'
*    ).
  ENDMETHOD.

  METHOD assert_equals_float.
    cl_abap_unit_assert=>assert_equals(
        act =  CONV f( '12.12345 ')
        exp =  CONV f( '12.12345 ')
        msg ='Assert!'
    ).
    cl_abap_unit_assert=>assert_equals_float(
        act =  CONV f( '12.12345 ')
        exp =  CONV f( '12.12345 ')
        msg ='Assert float!'
    ).
  ENDMETHOD.
ENDCLASS.
