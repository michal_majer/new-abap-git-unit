REPORT z_reduce_operator.

TYPES: BEGIN OF ty_invoice,
         number   TYPE vbeln,
         position TYPE posnr,
         ftype TYPE fkart,
         net   TYPE netwr,
         vat   TYPE netwr,
         gross TYPE netwr,
       END OF ty_invoice.

TYPES: lt_t_invoice TYPE SORTED TABLE OF ty_invoice WITH UNIQUE KEY number.

DATA: lt_invoice TYPE lt_t_invoice,
      lt_invoice_fkart TYPE lt_t_invoice,
      lt_invoice_z001 TYPE lt_t_invoice,
      lt_invoice_gross TYPE lt_t_invoice.

lt_invoice = VALUE lt_t_invoice(
                      ( number = '1000000000' position = '000010' ftype = 'Z001' net = '1000' vat = '230' )
                      ( number = '1000000000' position = '000011' ftype = 'Z001' net = '50' vat = '2' )
                      ( number = '1000000001' position = '000010' ftype = 'Z002')
                      ( number = '1000000002' position = '000010' )
                      ( number = '1000000003' position = '000010' ftype = 'Z001' net = '1000' vat = '230' )
                     ).

lt_invoice_gross = VALUE #( FOR ls_invoice IN lt_invoice ( number = ls_invoice-number
                                                              position = ls_invoice-position
                                                                 ftype = ls_invoice-ftype
                                                                   net = ls_invoice-net
                                                                   vat = ls_invoice-vat
                                                                 gross = ls_invoice-net + ls_invoice-vat ) ).

DATA(lv_gross_sum) = REDUCE netwr( INIT sum = 0 FOR ls_invoice IN lt_invoice_gross WHERE ( ftype = 'Z001' ) NEXT sum = sum + ls_invoice-gross ).
DATA(lv_quantity)  = REDUCE i( INIT quantity = 0 FOR ls_invoice IN lt_invoice_gross WHERE ( ftype = 'Z001' ) NEXT quantity = quantity + 1 ).
