REPORT z_tables.



TYPES: BEGIN OF lt_s_spfli,
         carrid   TYPE spfli-carrid,
         connid   TYPE spfli-connid,
         cityfrom TYPE spfli-cityfrom,
         cityto   TYPE spfli-cityto,
       END OF lt_s_spfli.


DATA lt_spfli TYPE SORTED TABLE OF lt_s_spfli WITH UNIQUE KEY carrid connid.

lt_spfli = VALUE #( ( carrid = 'LH' connid = 0001 cityfrom = 'Poznan' cityto = 'Warszawa' )
                     ( carrid = 'LH' connid = 0002 cityfrom = 'Poznan' cityto = 'Krakow' )
                     ( carrid = 'LH' connid = 0003 cityfrom = 'Warszawa' cityto = 'Gdansk' )
                     ( carrid = 'LO' connid = 0001 cityfrom = 'Warszawa' cityto = 'Gdansk' ) ).


LOOP AT lt_spfli INTO DATA(ls_spfli)
    GROUP BY ( key1 = ls_spfli-carrid ) INTO DATA(key_lh).
  LOOP AT GROUP key_lh INTO DATA(ls_member).

  ENDLOOP.
ENDLOOP.


LOOP AT lt_spfli INTO ls_spfli
    GROUP BY ( key1 = ls_spfli-carrid key2 = ls_spfli-connid ) INTO DATA(key).
  LOOP AT GROUP key INTO ls_member.

  ENDLOOP.
ENDLOOP.

DATA(ls_index) = lt_spfli[ 1 ].
lt_spfli[ 1 ]-cityto = 'Szczecin'.
DATA(ls_read) = lt_spfli[ carrid = 'LH' ].
ls_read = lt_spfli[ carrid = 'LH' connid = 0002 ].
DATA(lv_cityto) = lt_spfli[ carrid = 'LH' connid = 0002 ]-cityto.


TRY.
    DATA(ls_error) = lt_spfli[ 0 ].
  CATCH cx_sy_itab_line_not_found.
    WRITE: 'Table index starts with 1'.
ENDTRY.

IF line_exists( lt_spfli[ 0 ] ).
    WRITE: /, 'Line exists'.
ELSE.
    WRITE: /, 'Line not exists!'.
ENDIF.
