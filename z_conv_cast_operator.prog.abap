REPORT z_conv_cast_operator.

DATA(lv_float) = '5.00'.
DATA(lv_int) = CONV i( lv_float ).

*DATA: lv_ticket_guid TYPE crmt_object_guid VALUE '2FEBBA43AF88E71180C700505697183A'.
DATA(lv_string_ticket_guid) = '2FEBBA43AF88E71180C700505697183A'.
DATA(lv_ticket_guid) = CONV crmt_object_guid( lv_string_ticket_guid ).

*CALL FUNCTION 'CRM_ORDER_READ'
*    EXPORTING
*        it_header_guid =  VALUE crmt_object_guid_tab( ( lv_string_ticket_guid ) ).

CALL FUNCTION 'CRM_ORDER_READ'
  EXPORTING
    it_header_guid = VALUE crmt_object_guid_tab( ( CONV #( lv_string_ticket_guid ) ) ).

CALL FUNCTION 'CRM_ORDER_READ'
  EXPORTING
    it_header_guid = VALUE crmt_object_guid_tab( ( lv_ticket_guid ) ).
